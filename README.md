# oMailgw API - Gestion de passerelle(s) e-mail sortante(s)

**Lire la documentation générale : https://framagit.org/omailgw/omailgw-doc**

**Ce qui se trouve ci-dessous ne traite que de la partie API**

Offre et support commercial assuré par retzo.net : https://retzo.net/services/hebergement/passerelle-e-mail-relai-smtp/

## Pré-requis 

Les pré-requis sont celles de Laravel : https://laravel.com/docs/10.x/deployment#server-requirements 

La base de donnée supporté est Mysql/Mariadb. Il n'est pas exclu que ça tombe en marche avec Postgresql mais non testé. Le support de sqlite n'est plus à partir de la v0.9.

## Installation de oMailgw-API

Le pré-requis est d'avoir un serveur apache (htaccess) + **php8.1** (minimum) + mysql/mariadb prêt à l'emploi. Pour de la production un service http (type apache) est largement conseillé. 

Récupération des sources

```bash
cd /var/www
git clone https://framagit.org/omailgw/omailgw-api.git
cd omailgw-api
cp .env.example .env # + édit .env
```

Editer le fichier .env avec vos variables d’environnement. Notamment pour la base de données 

Exemple pour mysql : 

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=omailgw
DB_USERNAME=omailgw
DB_PASSWORD=********************
```

```bash
composer install
php artisan key:generate
php artisan migrate
# Génère l'utilisateur root + de faux log si l’environnement APP_ENV n'est pas "prod"
php artisan db:seed 
```

Mettre le répertoire courant (ici /var/www/omailgw-api) disponible en HTTP. Si vous n'avez pas de service http et que vous voulez tester l'application utilisez : 

```bash
php artisan serve
```

Tâche planifié quotidienne qui permet de : 
* Nettoyer les logs (variable CLEAN_LOG_LIMIT en jours)
* Lancer l'envoi des rapports journaliers
    * Lancement manuel : `php artisan report:send`

```
*/15 * * * * php cd /var/www/omailgw-api/artisan schedule:run >> /dev/null 2>&1
```

## Documentation

Toute la documentation se trouve au format OpenApi: 3.0.0 : https://app.swaggerhub.com/apis/retzo.net/oMailgw/

Exemple d'authentification + création d'utilisateur en ligne de commande avec CURL : 

```bash
curl -X POST \
  'http://127.0.0.1:8000/api/user/login?password=password&email=admin%40retzo.net' \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json' 
# Renvoi :
#{
#  "token": "34|mM5UuSfhhkHBKPY9UUqCoSn86y26CPhLtuZUJD2a",
#  "role": "user"
#}
# Récupérer "token"
curl -X POST \
  'http://127.0.0.1:8000/api/admin/user/add' \
  --header 'Accept: application/json' \
  --header 'Authorization: Bearer 34|mM5UuSfhhkHBKPY9UUqCoSn86y26CPhLtuZUJD2a' \
  --header 'Content-Type: application/json' \
  --data-raw '{
  "email": "contact@client.fr",
  "password": "superMotDePasse",
  "role": "user",
  "rapport": 0
}'
```

### Blackliste

Des règles de blacklistes peuvent être ajoutés. Elles peuvent être attribuées aux champs "from" ou aux champs "to". Pour modifier leur comportement modifier le fichier config/rules.php

Ici la règle "UserUnknow" (nom) applique une blackliste sur le "to" si :

* Cette règle a été appliquée plus de 2 fois (au bout de la 3ème) (en attendant son statut est en "greylist")
* Avec les conditions suivantes 
  * Le smtp_status n'est pas à sent 
  * Le smtp_msg correspond à la liste énoncée 
    * "Requested mail action aborted, mailbox not found"
    * ...

```php
# A appliquer sur le champs "to"
'BlacklistRuleTo' => [
	'UserUnknown' => [
		'nb' => '2',
		'columns' =>[
			'smtp_status' => [
				'!=', 
				[
					'sent',
				],
			],
			'smtp_msg'=>[ 
				'like', 
				[
					'%Recipient address rejected: this mailbox is inactive and has been disabled (in reply to RCPT TO command)',
					'%Recipient address rejected: unverified address%(in reply to RCPT TO command)',
					'%Requested mail action aborted, mailbox not found%',
					'%rejected: user unknown (in reply to RCPT TO command)',
					'%Recipient address rejected: User unknown%(in reply to RCPT TO command)',
					'%Requested action not taken: mailbox unavailable%(in reply to RCPT TO command)',
					'%cannot be delivered. This mailbox is disabled %(in reply to end of DATA command)',
					'This mailbox has been blocked due to inactivity%(in reply to RCPT TO command)',
				],
			],
		],
	],
],
```



## Utilisateurs/rôles

Les utilisateurs par défaut : 

* Utilisateur :
  
  * root@retzo.net (Rôle root)
  
  * admin@retzo.net (Rôle admin)
  
  * user@retzo.net (Rôle : user)
* Mot de passe  : password

## Démonstration

API de démonstration : 

* URL : https://omailgw-demo.retzo.net/
* URL API : https://omailgw-demo.retzo.net/api/
* identifiant ci-dessus

## Auteurs

* Auteur : [David Mercereau](https://david.mercereau.info/) [@kepon](https://framagit.org/kepon)
* Contributeur : [Pierrick Anceaux](https://github.com/Shurtugl/) [@Shurtugl](https://framagit.org/Shurtugl) 

## Licence

Licence GPL v3 http://www.gnu.org/licenses/gpl-3.0.html

Voir Licence.txt
