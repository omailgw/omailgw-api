<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\LogController;

class LogClean extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'log:clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean old log (env : CLEAN_LOG_LIMIT)';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        LogController::clean();
    }
}
