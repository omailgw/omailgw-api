<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\LogController;

class LogDuplicateLogSmtps extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'log:dupli';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Log duplicate logs>log_smtps table information (from/smtpd_username)';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        LogController::duplicateDataLogSmtps();
    }
}
