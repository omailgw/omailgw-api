<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\LogController;

class LogRebound extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'log:rebound {after}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rebound detect and traitment';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $after = $this->argument('after');
        if ($after == 0 || $after == null) {
            LogController::rebound();
        } else {
            LogController::rebound(null,    time()-$after);
        }
    }
}
