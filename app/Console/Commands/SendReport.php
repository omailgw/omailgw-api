<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\Mail\Report;
use App\Models\User;
use App\Models\Log;
use Illuminate\Support\Facades\Log as LaravelLog;

class SendReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send daly report to user';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        LaravelLog::debug( "Report send task launch");
        $users = User::where('rapport', 1)->get();
        if ($users->count() > 0) {

            foreach ($users as $user) {

                LaravelLog::debug( "For user ".$user['email']);
                $req['type']=['countTotal', 'countInError', 'countOutError', 'topToError', 'topFromError','topToDomain', 'topToDomainError', 'topFromDomain', 'topFromDomainError'];
                $req['limit']=20;
                // -- Baliser les dates
                $sendReport=false;
                // Rapport Quotidien
                if ($user['rapport'] == 1) {
                    $req['dateStart']=strtotime("-1 day");
                    $req['dateEnd']=time();
                    $sendReport=true;
                // Rapport Hebdo
                } elseif ($user['rapport'] == 2 && date('N') == 1) {
                    $req['dateStart']=strtotime("-1 week");
                    $req['dateEnd']=time();
                    $sendReport=true;
                // Rapport Mensuelle
                } elseif ($user['rapport'] == 3 && date('j') == 1 ) {
                    $req['dateStart']=strtotime("-1 month");
                    $req['dateEnd']=time();
                    $sendReport=true;
                }

                // Rapport verbose 0=juste les erreurs 1=tout
                $sendReportVerbose0=false;
                if ($sendReport == true && $user['rapport_verbose'] == 0) {
                    $req['type']=['countInError', 'countOutError'];
                    $log = New Log;
                    $logStat = $log->stat($req, $user['id'], $user['role']);
                    if ($logStat['countInError'] != 0 || $logStat['countOutError'] != 0) {
                        $req['type']=['countTotal', 'countInError', 'countOutError', 'topOutError', 'topInError', 'topOutDomainError', 'topOutEmailError'];
                        $sendReportVerbose0=true;
                    }
                }
                // Envoi du rapport
                if (
                        ($user['rapport'] != 0 && $sendReport == true && $user['rapport_verbose'] == 1)
                    ||
                        ($user['rapport'] != 0 && $sendReport == true && $user['rapport_verbose'] == 0 && $sendReportVerbose0 == true)
                    ) {
                    LaravelLog::debug( "- debug : startDate fir report : ".date('Y-m-d', $req['dateStart']));
                    LaravelLog::debug( "- debug : dateEnd fir report : ".date('Y-m-d', $req['dateEnd']));
                    $log = New Log;

                    $logStat = $log->stat($req, $user['id'], $user['role']);
                    if ($logStat['countTotal'] != 0) {
                        $logStat=json_decode(json_encode($logStat), true);
                        Mail::to($user)->send(new Report($user, $logStat, $req));
                        LaravelLog::info( "Send report to ".$user['email']);
                    }else {
                        LaravelLog::debug( "No e-mail in log, no report for ".$user['email']);
                    }
                }
            }
        }

        //return true;
    }
}
