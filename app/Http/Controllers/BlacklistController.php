<?php

namespace App\Http\Controllers;

use App\Models\Blacklist;
use App\Models\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log as LaravelLog;
use Datetime;



class BlacklistController extends Controller
{
    /*
    *   Search e-mail in log
    */
    private function inLog($email, $type)
    {
        if ($type == 1) { // From
            $inLog = DB::table('logs');
            $inLog->selectRaw('from');
            $addLogPermission = new Log;
            $addLogPermission->addLogPermission($inLog, 'logs');
            $inLog->where("from", $email);
            $inLog->distinct();
        } else if ($type == 2) { // To
            $inLog = DB::table('log_smtps');
            $inLog->selectRaw('smtp_to');
            $addLogPermission = new Log;
            $addLogPermission->addLogPermission($inLog, 'log_smtps');
            $inLog->where("smtp_to", $email);
            $inLog->distinct();
        }
        return $inLog->count();
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $auth = auth()->user();
        // N'afficher que les blacklists qui me concerne selon mes log_permission
        $BlacklistQuery = DB::table('blacklists')
            ->selectRaw('id, email, status, type, rules, rules_count, user_id, created_at, updated_at, (SELECT count(id) FROM blacklists b2 WHERE b2.type = blacklists.type AND b2.email =  blacklists.email AND b2.status = 0 AND blacklists.status != 0) disableByWhitelist');
        // Si rôle "root|admin" on affiche tout (donc aucune permission appliqué)
        if ($auth->role != "root") {
            $BlacklistQuery->whereIn('email', function($query) {
                $query->select('smtp_to')->from('log_smtps');
                $addLogPermission = New Log;
                $addLogPermission->addLogPermission($query, 'log_smtps');
                $query->distinct();
            });
            $BlacklistQuery->orWhereIn('email', function($query) {
                $query->select('from')->from('logs');
                $addLogPermission = New Log;
                $addLogPermission->addLogPermission($query, 'logs');
                $query->distinct();
            });
        }
        $Blacklist = $BlacklistQuery->get();
        return response($Blacklist, 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $auth = auth()->user();
        $BlacklistData = $request->validate([
            'email' => ['required', 'email', 'min:4', 'max:255'],
            'status' => ['required', 'integer', 'min:0', 'max:2'],
            'type' => ['required', 'integer', 'min:1', 'max:2'],
        ]);
        $inLog = 1;
        if ($auth->role != "root") {
            $inLog = $this->inLog($BlacklistData['email'], $BlacklistData['type']);
        }
        if ($inLog != 0) {
            $alreadyExist=Blacklist::where("email", $BlacklistData['email'])
                ->where("status", $BlacklistData['status'])
                ->where("type", $BlacklistData['type'])
                ->count();
            if ($alreadyExist != 0) {
                return response(['message' => 'Already exist'], 405);
            } else {
                $Blacklist = Blacklist::create([
                    "email" => $BlacklistData['email'],
                    "status" => $BlacklistData['status'],
                    "type" => $BlacklistData['type'],
                    "user_id" => $auth->id
                ]);
                return response($Blacklist, 201);
            }
        } else {
            return response(['message' => 'Forbidden, This e-mail is not mentioned in your logs to which you have access'], 403);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Blacklist $blacklist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Blacklist $blacklist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Blacklist $blacklist, Request $request)
    {
        $auth = auth()->user();
        $id = request()->route('id');
        $Blacklist = Blacklist::where("id", $id)->first();
        if (empty($Blacklist)) {
            return response(['message' => "Not Found"], 404);
        }
        $inLog = 1;
        if ($auth->role != "root") {
            $inLog = $this->inLog($Blacklist['email'], $Blacklist['type']);
        }
        if ($inLog != 0) {
            $BlacklistDestroy = Blacklist::destroy($id);
            if ($BlacklistDestroy == 0) {
                return response(["Message" => "Not found"], 404);
            } else {
                return response(["Message" => "Ok"], 200);
            }
        } else {
            return response(['message' => 'Forbidden, This e-mail is not mentioned in your logs to which you have access'], 403);
        }
    }

    /**
     * Automatically apply blacklisting rules
     */
    public function Learn($queueId, $process)
    {
        LaravelLog::debug("Appel de Learn pour l'id : " . $queueId);
        // Lire les rules depuis le fichier config
        $rulesCfg = Config::get('rules');
        $rulesLog = "";
        // Lire les log à partir de divers champs d'origine
        foreach ($rulesCfg  as $ruleSide => $rules) {
            $side = $ruleSide == 'BlacklistRuleFrom' ? 'from' : 'smtp_to';
            //LaravelLog::debug("process= " . $process . " , regle actuelle porte sur = " . $side);
            if (($side == 'from' && $process == 'smtpd')||($side == 'smtp_to' && $process == 'smtp')){
                //LaravelLog::debug("Le process correspond, traitement...");
                //Verifier si le log correspond à une rule
                foreach ($rules as $ruleName => $ruleValue) {
                    //LaravelLog::debug("Prépare la règle " . $ruleName);
                    $rulesLog .= $ruleName.", ";
                    $trigger = $ruleValue['nb'];
                    $columns = $ruleValue['columns'];
                    //construire une requete sur le log en cours
                    $log = DB::table('logs')
                        ->leftJoin('log_smtps', 'logs.queueId', '=', 'log_smtps.smtp_queueId')
                        ->where('logs.queueId', $queueId);
                    //ajouter à la requete les elements par colonne de la $rule en cours
                    foreach ($columns as $columnName => $columnValues) {
                        $log->where(function ($query) use ($columnName, $columnValues) {
                            //dans cette colonne, tester chacune des valeurs en règle avec l'opérateur de la règle
                            foreach ($columnValues[1] as $value) {
                                $query->orwhere($columnName, $columnValues[0], $value); //sous requetes "orwhere" la moindre valeur trouvé valide le test
                            }
                        });
                    }
                    //LaravelLog::debug("Testé la règle \"" . $ruleName . "\"");
                    $logResult = $log->select($side)->get(); //obtenir le resultat
                    // si resultat non vide, ce log correspond à la règle donc l'ajouter au compteur (greylist)
                    if (!$logResult->isEmpty()) {
                        $now = new Datetime();
                        $email = json_decode($logResult, true)[0][$side];
                        LaravelLog::warning("La règle \"" . $ruleName . "\" est déclenchée pour l'adresse \"" . $email . "\"");
                        // trouver la ligne de la table blacklist qui porte sur l'email du log ET sur la rule en cours
                        $blacklist = DB::table('blacklists')
                            ->where('email', $email)
                            ->where('rules', 'like', '%' . $ruleName . '%')
                            ->first();
                        // si pas de ligne déjà existante, la créer
                        if (!$blacklist) {

                            DB::table('blacklists')->insert([
                                'email' => $email,
                                'status' => 1, //ajouté en greylist, comptage en cours
                                'type' => $side == 'from' ? 1 : 2,
                                'rules' => $ruleName,
                                'rules_count' => 1, //compteur incrémenté
                                'created_at' => $now,
                                'updated_at'=> $now
                            ]);
                            //LaravelLog::debug("Nouveau compteur greylist créé");
                            // si ligne existante, incrémenter le compteur
                        } else {
                            $rulesCount = $blacklist->rules_count + 1;
                            if ($blacklist->status == 0) {
                                //LaravelLog::debug("Email en whiteList");
                            } else if ($rulesCount >= $trigger) {
                                LaravelLog::warning("Email blacklisté");
                                $status = 2;
                            } else {
                                //LaravelLog::debug("Email en greylist");
                                $status = 1;
                            }

                            DB::table('blacklists')
                                ->where('id', $blacklist->id)
                                ->update([
                                    'rules_count' => $rulesCount,
                                    'status' => $status,
                                    'updated_at'=> $now
                                ]);
                        }
                    } else {
                        //LaravelLog::debug("La règle " . $ruleName . " n'a rien déclenché.");
                    }
                }
            }
        }
        LaravelLog::debug("Regles utilisées : " . $rulesLog);
    }
}
