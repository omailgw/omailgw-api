<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use DataTables;
use App\Models\Log;
use App\Models\LogSmtp;
use App\Models\Server;
#use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log as LaravelLog;
use App\Http\Controllers\BlacklistController as BList;

class LogController extends Controller
{
    // Champs de donnée Log valide pour l'utilisateur
    protected $dataLog = [
        'queueId',
        'server',
        'from',
        'to',
        'message_id',
        'smtpd_client',
        'smtpd_date',
        'smtpd_username',
        'smtpd_codeError',
        'smtpd_msg',
        'cleanup',
        'qmgr_size',
        'qmgr_nrcpt',
        'qmgr_msg',
        'msg',
        'smtp_to',
        'smtp_date',
        'smtp_relay',
        'smtp_status',
        'smtp_msg'
    ];
    // Champs de donnée Log caché pour l'utilisateur
    protected $dataLogHide = [
        'process',
        'smtp_queueId',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index()
    {
        //
    }*/

    /**
     * Valid date with format
     *
     * @param  $Date, $Format
     * @return boolean
     */
    private static function validateDate($date, $format = 'Y-m-d')
    {
        $d = \DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }

    /*
     * Dupliquer les champs smtpd_username/from de la table logs vers log_smtps
     */
    public static function duplicateDataLogSmtps($queueId = null)
    {
        LaravelLog::debug("duplicateDataLogSmtps(".$queueId.")");
        if ($queueId != null) {
            // Dans le cas ou le QUEUEID est spécifier
            $Logs = Log::select('smtpd_username', 'from')->where('queueId', $queueId)->first();
            if (isset($Logs)) {
                LogSmtp::where('smtp_queueId', $queueId)->update(
                    [
                        'smtpd_username' => $Logs['smtpd_username'],
                        'from' => $Logs['from']
                    ]
                );
            }
        } else {
            // Dans le cas ou il n'y a pas de QUEUEID on met tout à jour si c'est null
            DB::statement("UPDATE log_smtps
                SET `smtpd_username` = (
                        SELECT `smtpd_username`
                        FROM logs T2
                        WHERE T2.queueId = log_smtps.smtp_queueId
                    )
                WHERE `smtpd_username` IS NULL;");
            DB::statement("UPDATE log_smtps
            SET `from` = (
                    SELECT `from`
                    FROM logs T2
                    WHERE T2.queueId = log_smtps.smtp_queueId
                )
            WHERE `from` IS NULL;");
            DB::statement("UPDATE log_smtps
            SET `server` = (
                    SELECT `server`
                    FROM logs T2
                    WHERE T2.queueId = log_smtps.smtp_queueId
                )
            WHERE `server` IS NULL;");
        }
    }

    /*
    * Si c'est un rebond de serveur alors on duplique certaine information
    */
    public static function rebound($queueId = null, $afterDate = null) {
        LaravelLog::debug("rebound(".$queueId.", ".$afterDate.")");
        // Recherche des rebond vers un server smtp_relay connu
        $logSmtpOrigins = LogSmtp::select('smtp_queueId', 'smtp_msg', 'smtpd_username')
            ->where('rebound', 0);
        // On limite à la queueId
        if ($queueId != null) {
            $logSmtpOrigins->where('smtp_queueId', $queueId);
        }
        // On limite dans le temps
        if ($afterDate != null) {
            $logSmtpOrigins->where('smtp_date', '>', $afterDate);
        }
        // Recherche par serveur connu dans le champs smtp_relay
        $logSmtpOrigins->where(function($logSmtpOrigins) {
            foreach (Server::get() as $Server) {
                $logSmtpOrigins->orWhere('smtp_relay', 'like', $Server['hostname'].".".$Server['domain'].'%');
            }
        });
        /*$sql = $logSmtpOrigins->toSql();
        $bindings = $logSmtpOrigins->getBindings();
        $sql_with_bindings = preg_replace_callback('/\?/', function ($match) use ($sql, &$bindings) {
            return json_encode(array_shift($bindings));
        }, $sql);
        dd($sql_with_bindings);*/
        foreach ($logSmtpOrigins->get() as $logSmtpOrigin) {
            $preg_match=preg_match('/as (?<queueId>[A-Z0-9]+)/', $logSmtpOrigin['smtp_msg'], $logSmtpDestination);
            if($preg_match == 1) {
                LaravelLog::debug("Copie des informations de ".$logSmtpOrigin['smtp_queueId']." vers ".$logSmtpDestination['queueId']." (si existe)");
                // Marqué comme "rebond' sur l'original
                Log::where('queueId', $logSmtpOrigin['smtp_queueId'])->update( [
                    'rebound' => 1
                ] );
                LogSmtp::where('smtp_queueId', $logSmtpOrigin['smtp_queueId'])->update( [
                    'rebound' => 1
                ] );
                // Copier le nescécaire sur la destination
                Log::where('queueId', $logSmtpDestination['queueId'])->update( [
                    'smtpd_username' => $logSmtpOrigin['smtpd_username']
                ] );
                LogSmtp::where('smtp_queueId', $logSmtpDestination['queueId'])->update( [
                    'smtpd_username' => $logSmtpOrigin['smtpd_username']
                ] );
            }
        }
    }

    /**
     * Add Log in database
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (config('app.debug'))
            DB::connection()->enableQueryLog();
        $valideData = array_merge($this->dataLog, $this->dataLogHide);
        // Récupération de la liste des serveurs pour gérer les rebonds
        $Servers = Server::get();
        // Traitement des données
        $data_processed = 0;
        $data_ignored = 0;
        $learningID = 0;
        $learningProcess = "";
        foreach ($request->all()  as $data) {

            // Suppression des données indésirables
            foreach ($data as $key => $value) {
                if (!in_array($key, $valideData)) {
                    LaravelLog::warning("La donnée vient d'être nettoyé : " . $data[$key]);
                    unset($data[$key]);
                }
            }
            // Traitement d'un jeu de donnée
            LaravelLog::debug("Traitement de la donnée " . json_encode($data));
            // SMTP peut avoir plusieurs ligne avec le même queueId, il nous faut la dernière en date...
            if ($data['process'] == 'smtp' || $data['process'] == 'lmtp' || $data['process'] == 'error') {
                $learningProcess = $data['process'];
                unset($data['process']);
                // Mise en forme de smtp_date_day
                $data['smtp_date_day']=date('Y-m-d', $data['smtp_date']);
                // Vérification de l’existance de la donnée préalable
                $selectDuplicate = LogSmtp::where("smtp_queueId", $data['smtp_queueId'])
                    ->where("smtp_to", $data['smtp_to']);
                if ($selectDuplicate->count() == 0) {
                    LaravelLog::debug("Ajout de la data (smtp)");
                    LogSmtp::create($data);
                    // Dupliquer le champs smtpd_username de la table logs vers log_smtps
                    $this->duplicateDataLogSmtps($data['smtp_queueId']);
                    $learningID = $data['smtp_queueId'];
                } else {
                    $selectDuplicateData=$selectDuplicate->addSelect('id')->first();
                    LogSmtp::where('id', $selectDuplicateData['id'])
                        ->update($data);
                    LaravelLog::debug("La data (smtp_queueId+smtp_to) est déjà présente, on l'a met à jour (smtp)");
                }
                // Gestion rebound de server connu à server connu
                if (isset($data['smtp_relay'])) {
                    foreach ($Servers as $Server) {
                        if (preg_match("/^".$Server['hostname'].".".$Server['domain']."/", $data['smtp_relay'])) {
                            LaravelLog::debug("C'est un rebond, on appel la fonction rebound(queueId)");
                            $this->rebound($data['smtp_queueId']);
                        }
                    }
                }
                // Les autres process ont le même traitement
            } elseif (
                $data['process'] == 'cleanup' ||
                $data['process'] == 'qmgr' ||
                $data['process'] == 'pickup' ||
                $data['process'] == 'smtpd'
            ) {
                $learningProcess = $data['process'];
                unset($data['process']);
                // Mise en forme de smtpd_date_day
                if (isset($data['smtpd_date'])) {
                    $data['smtpd_date_day']=date('Y-m-d', $data['smtpd_date']);
                }

                // Vérification de l’existance préalable de la QueueId
                // Si c'est une queueId généré il faut comparer d'autres champs
                if (preg_match("/^NOQ/", $data['queueId'])) {
                    $selectDuplicate = Log::where("smtpd_date_day", $data['smtpd_date_day'])
                        ->where("to", $data['to'])
                        ->where("from", $data['from']);
                } else {
                    $selectDuplicate = Log::where("queueId", $data['queueId']);
                }
                if ($selectDuplicate->count() == 0) {
                    LaravelLog::debug("La QueueID n'existe pas, on ajoute)");
                    Log::create($data);
                    $learningID = $data['queueId'];
                } else {
                    unset($data['queueId']); // Pour conserver la première queueId
                    $recupSelec=$selectDuplicate->first();
                    // Si la QueueId est déjà présente, on met a jour avec les autres données
                    LaravelLog::debug("La QueueID est présente, on la met à jour  ".$recupSelec['queueId']);
                    DB::table('logs')
                        ->where('queueId', $recupSelec['queueId'])
                        ->update($data);
                    $learningID = $recupSelec['queueId'];
                }
            } else {
                $data_ignored++;
                LaravelLog::warning("La donnée vient d'être ignoré : " . json_encode($data));
            }
            //appliquer l'apprentissage blacklist sur la donnée qui vient d'être enregistrée
            if ($learningID != 0) {
                (new BList)->Learn($learningID,$learningProcess);
            }
            $data_processed++;
        }
        $response = [
            'message' => "Ok",
            'data_processed' => $data_processed,
            'data_ignored' => $data_ignored
        ];
        if (config('app.debug')) {
            $query = DB::getQueryLog();
            $response['debug']=$query;
        }
        // Affichage de la réponse
        return response(
            $response,
            200
        );
    }


    /**
     * Clean old log with env.CLEAN_LOG_LIMIT
     *
     * @return \Illuminate\Http\Response
     */
    public static function clean()
    {
        LaravelLog::debug("clean()");
        // Suppression des données top anciennes
        // Variable CLEAN_LOG_LIMIT
        $dateMenage = strtotime(date("Y-m-d") . '- ' . config('config.CLEAN_LOG_LIMIT') . ' days');
        $deleteNb = Log::where('smtpd_date', '<', $dateMenage)->delete();
        LogSmtp::where('smtp_date', '<', $dateMenage)->delete();
        if (!app()->runningInConsole()) {
            return response(['message' => "Ok", "delete" => $deleteNb], 200);
        } else {
            return null;
        }
    }

    /**
     * Parse by queueId
     */
    public function show()
    {
        // Récupération du paramètre QueueId dans la route
        $queueId = request()->route('queueId');
        // Initialisation de la base
        $logQuery = DB::table('logs');
        // Ajout des permissions de log
        $addLogPermission = new Log;
        $addLogPermission->addLogPermission($logQuery, 'logs');
        // Fin de la requêtes
        $logQuery->where("logs.queueId", $queueId);
        $logQuery->leftJoin('log_smtps', 'logs.queueId', '=', 'log_smtps.smtp_queueId');
        $select = '`logs`.`server` `server`, `queueId`, `logs`.`from` `from`, `smtpd_date`, `smtpd_client`, `logs`.`smtpd_username` `smtpd_username`, `smtpd_codeError`, `smtpd_msg`, `message_id`, `cleanup`, `qmgr_size`, `qmgr_nrcpt`, `qmgr_msg`, `msg`, `smtp_date`, `smtp_relay`, `smtp_status`, `smtp_msg`';
        $logQuery->select(DB::raw($select . ', CONCAT(COALESCE(`to`, ""), " ", COALESCE(`smtp_to`, "")) as `to`'));
        // Exécution de la requête
        $resultLogQuery = $logQuery->get();
        // Affichage de la réponse
        if ($resultLogQuery->count() != 0) {
            return response($resultLogQuery, 200);
        } else {
            return response(["message" => "Message not found"], 404);
        }
    }

    /**
     * Display the logs with DataTables
     *
     * @param  \App\Models\Log  $log
     * @return \DataTables
     * Doc : https://github.com/yajra/laravel-datatables-docs
     */
    public function parseDataTables(Request $request)
    {
        // Initialisation de la requête
        $logQuery = DB::table('logs');
        $logQuery->leftJoin('log_smtps', 'logs.queueId', '=', 'log_smtps.smtp_queueId');
        // Ajout des permissions de log
        $logClass = new Log;
        $logClass->addLogPermission($logQuery, 'logs');
        // Construction du select
        foreach ($request->columns as $columns) {
            if ($columns['data'] == "smtp_to") {
                //Affichage sur les 2 colonnes TO concaténé
                $logQuery->addSelect(DB::raw('CONCAT(COALESCE(`to`, ""), " ", COALESCE(`smtp_to`, "")) as `smtp_to`'));
            } else if ($columns['data'] == "server") {
                $logQuery->addSelect(DB::raw('`log_smtps`.`server` `server`'));
            } else if ($columns['data'] == "from") {
                $logQuery->addSelect(DB::raw('COALESCE(`log_smtps`.`from`, `logs`.`from`) AS `from`'));
            } else if ($columns['data'] == "smtpd_username") {
                $logQuery->addSelect(DB::raw('`log_smtps`.`smtpd_username` `smtpd_username`'));
            } else {
                $logQuery->addSelect($columns['data']);
            }
        }
        // Filtre par type
        if ($request->type != '') {
            $logClass->addLogType($logQuery, $request->type);
        }
        // Recherche par date si valide & si complété
        if (
            $request->frmDate != '' && $request->toDate != ''
            && $this->validateDate($request->frmDate) && $this->validateDate($request->toDate)
        ) {
            $logQuery->where('smtpd_date', ">=", strtotime($request->frmDate." 00:00:01"));
            $logQuery->where('smtpd_date', "<=", strtotime($request->toDate." 23:59:59"));
        }
        return DataTables::of($logQuery)
            ->toJson();
    }

    /**
     * Display the log (search)
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        if (config('app.debug'))
            DB::connection()->enableQueryLog();
        // Colonne valide pour la recherche
        $columnCheck = array_merge($this->dataLog, ['date']);
        // Opération valide pour la recherche
        $operatorCheck = [
            'like',
            '=',
            '!=',
            '<>',
            '<',
            '<=',
            '>',
            '>='
        ];
        // Initialisation de la requête
        $query = DB::table('logs');

        $query->leftJoin('log_smtps', 'logs.queueId', '=', 'log_smtps.smtp_queueId');
        $select = '`logs`.`server` `server`, `queueId`, `logs`.`from` `from`, `smtpd_date`, `smtpd_client`, `logs`.`smtpd_username` `smtpd_username`, `smtpd_codeError`, `smtpd_msg`, `cleanup`, `qmgr_size`, `qmgr_nrcpt`, `qmgr_msg`, `msg`, `smtp_date`, `smtp_relay`, `smtp_status`, `smtp_msg`';
        $query->select(DB::raw($select . ', CONCAT(COALESCE(`to`, ""), " ", COALESCE(`smtp_to`, "")) as `to`'));
        // Ajout des permissions de log
        $addLogPermission = new Log;
        $addLogPermission->addLogPermission($query, 'logs');
        // Ajout d'une limit
        if (is_integer($request->limit)) {
            $query->take($request->limit);
        } else {
            $query->take(20);
        }
        // Ajout d'un offset
        if (is_integer($request->offset)) {
            $query->skip($request->offset);
        } else {
            $query->skip(0);
        }
        // Ajout d'un order by
        if (is_array($request->orderBy)) {
            if ($request->orderBy['column'] == 'date') {
                $query->orderBy('smtpd_date', $request->orderBy['order']);
            } else {
                $query->orderBy($request->orderBy['column'], $request->orderBy['order']);
            }
        }
        // Ajout d'une requête recherche
        if (is_array($request->search)) {
            foreach ($request->search as $search) {
                $search = array_values($search);
                $column = $search[0];
                $operator = $search[1];
                $value = $search[2];
                // Vérification des colonnes
                if (!in_array($column, $columnCheck)) {
                    return response(['message' => "Unknown column", "Valide column" => $columnCheck], 404);
                }
                // Vérification de l'opérateur
                if (!in_array($operator, $operatorCheck)) {
                    return response(['message' => "Unknown operator", "Valide operator" => $operatorCheck], 404);
                }
                // Compléter le "to" (récupérer dans logs.to & dans log_smtps.smtp_to)
                if ($column == "to") {
                    $query->where("logs.to", $operator, $value);
                    $query->orWhere("log_smtps.smtp_to", $operator, $value);
                } else if ($column == "date") {
                    $query->where("logs.smtpd_date", $operator, $value);
                    //$query->orWhere("log_smtps.smtp_date", $operator, $value);
                // Compléter le "msg"
                } elseif ($column == "msg") {
                    $query->where("logs.msg", $operator, $value);
                    $query->orWhere("logs.qmgr_msg", $operator, $value);
                    $query->orWhere("log_smtps.msg", $operator, $value);
                } else {
                    $query->where($column, $operator, $value);
                }
            }
        }
        //dd($result= $query->toSql());
        $result = $query->get();
        // Ajout du debug laravel si mon app est en mode debug
        if (config('app.debug')) {
            $query = DB::getQueryLog();
            $result['debug']=$query;
        }
        return $result;
    }

    /**
     * Log statistiques
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function stat(Request $request)
    {
        // Validation des données
        $requestData = $request->validate([
            'dateStart' => ['required', 'numeric'],
            'dateEnd' => ['required', 'numeric'],
            'limit' => ['numeric'], # Juste for TOP
            'type' => ['required', 'array', 'in:countTotal,countOutError,countInError,getByDay,getByMonth,topOutError,topInError,topInDomain,topOutDomain,topInDomainError,topOutDomainError,topInEmail,topOutEmail,topInEmailError,topOutEmailError'],
        ]);
        $log = new Log;
        $logStat = $log->stat($requestData);
        // Affichage de la réponse
        return response($logStat, 200);
    }
}
