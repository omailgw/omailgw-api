<?php

namespace App\Http\Controllers;

use App\Models\LogPermission;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class LogPermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $LogPermission = LogPermission :: all();
        return response($LogPermission, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_id = request()->route('id');
        $LogPermissionData = $request->validate([
            'field' => ['required', 'string', Rule::in(['from', 'from_dom', 'server', 'smtpd_username'])],
            'content' => ['required', 'string']
        ]);
        $logPermissions = LogPermission::create([
            "field" => $LogPermissionData['field'],
            "content" => $LogPermissionData['content'],
            "user_id" => $user_id
        ]);
        return response($logPermissions, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LogPermission  $logPermission
     * @return \Illuminate\Http\Response
     */
    public function show(LogPermission $logPermission)
    {
        // By request ID or auth ID
        $user_id = request()->route('id');
        if (empty($id)) {
            $id = auth()->user()->id;
        }
        $logPermissions = LogPermission::where("user_id", $user_id)->get();
        return response($logPermissions, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LogPermission  $logPermission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LogPermission $logPermission)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LogPermission  $logPermission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, LogPermission $logPermission)
    {
        $user_id = request()->route('id');
        $LogPermissionData = $request->validate([
            'id' => ['required', 'numeric']
        ]);
        $logPermissionsDelete = LogPermission::where("id", $LogPermissionData['id'])
                                        ->where("user_id", $user_id)
                                        ->delete();
        if ($logPermissionsDelete == 0) {
            return response(["Message" => "Not found"], 404);
        } else {
            return response(["Message" => "Ok"], 200);
        }
    }
}
