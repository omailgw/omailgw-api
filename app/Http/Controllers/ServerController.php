<?php

namespace App\Http\Controllers;

use App\Models\Blacklist;
use App\Models\Server;
use App\Models\Transport;
use App\Models\SmtpdAuth;
use App\Models\Spool;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log as LaravelLog;

class ServerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return response(Server::orderBy('hostname', "asc")->get(), 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $user_id = auth()->user()->id;
        $ServerData = $request->validate([
            'hostname' => ['required', 'string', 'min:4', 'max:40', 'regex:/^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$/i'],
            'domain' => ['required', 'string', 'min:4', 'max:40']
        ]);
        $Server = Server::create([
            "hostname" => $ServerData['hostname'],
            "domain" => $ServerData['domain'],
            "user_id" => $user_id
        ]);
        return response($Server, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Server $server)
    {
        $hostname = request()->route('hostname');
        $Server = Server::where("hostname", $hostname)->first();
        return response($Server, 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Server $server)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Server $server)
    {
        $hostname = request()->route('hostname');
        $ServerDelete = Server::where("hostname", $hostname)
                                ->delete();
        if ($ServerDelete == 0) {
            return response(["Message" => "Not found"], 404);
        } else {
            return response(["Message" => "Ok"], 200);
        }
    }

    /**
     * omailgw-cli communication
     * Doc :
     *  - https://framagit.org/omailgw/omailgw-cli/-/issues/2
     *  - https://app.swaggerhub.com/apis/retzo.net/oMailgw/#/cli/post-cli-hostname
     */
    public function cli(Request $request, Server $server)
    {
        $returnData = null;
        // Validate
        $hostname = request()->route('hostname');
        $requestValidate = $request->validate([
            'demand' => ['array'],
            'spool' => ['array']
        ]);

        // Find last_cli_update
        $Server = Server::select('id', 'last_cli_update')
                        ->where("hostname", $hostname)
                        ->first();
        if ($Server == null) {
            LaravelLog::warning("Un serveur tente de se connecter mais n'existe pas : ".$hostname);
            return response(["message" => "Unknow hostname"], 404);
        }

        // Spool gestion
        if (isset($requestValidate['spool'])) {
            $requestQueueId=array();
            foreach($requestValidate['spool'] as $key=>$spool) {
                // Récupération des QueueId en vu de la suppression des messages qui ne sont plus dans le spooler
                $requestQueueId[]=$spool['queueId'];
                // Ajout pour l'insert
                $requestValidate['spool'][$key]['server_id']=$Server['id'];
            }
            // Nettoyer le messages qui ne sont plus dans le spooler
            $SpoolDelete = Spool::whereNotIn('queueId',$requestQueueId)
                            ->where('server_id', $Server['id'])
                            ->delete();
            LaravelLog::debug("SpoolDelete : ".$SpoolDelete);
            // Ajouter les nouveaux messages qui ne sont pas dans le spooler
            $SpoolCreate = Spool::insertOrIgnore($requestValidate['spool']);
            LaravelLog::debug("SpoolCreate : ".$SpoolCreate);
        }

        // Return demand
        if (isset($requestValidate['demand'])) {
            foreach ($requestValidate['demand'] as $demand) {
                switch ($demand) {
                    case 'transport-change':
                        $TransportCount=Transport::where('created_at', '>', date('Y-m-d G:i:s', $Server['last_cli_update']))
                            ->where('server_id', $Server['id'])
                            ->count();
                        if ($TransportCount != 0) {
                            $Transport=Transport::select('search','search_regex','transport','nexthop')
                                ->where('server_id', $Server['id']);
                            $returnData['transport']= $Transport->get();
                        }
                        break;
                    case 'transport':
                        $Transport=Transport::select('search','search_regex','transport','nexthop')
                            ->where('server_id', $Server['id']);
                        $returnData['transport']= $Transport->get();
                        break;
                    case 'smtp-auth-change':
                        $SmtpAuthCount=SmtpdAuth::where('created_at', '>', date('Y-m-d G:i:s', $Server['last_cli_update']))->count();
                        if ($SmtpAuthCount != 0) {
                            $SmtpAuth=SmtpdAuth::select('username','password');
                            $returnData['smtp-auth']= $SmtpAuth->get();
                        }
                        break;
                    case 'smtp-auth':
                        $SmtpAuth=SmtpdAuth::select('username','password');
                        $returnData['smtp-auth']= $SmtpAuth->get();
                        break;
                    case 'blacklist-change':
                        $BlacklistCount=Blacklist::where('updated_at', '>', date('Y-m-d G:i:s', $Server['last_cli_update']))->count();
                        if ($BlacklistCount != 0) {
                            $BlacklistQuery = Blacklist::selectRaw('type, email, rules, user_id');
                            $BlacklistQuery->where("status", 2);
                            $BlacklistQuery->whereRaw("email NOT IN (SELECT email FROM blacklists b2 WHERE b2.type = blacklists.type AND b2.email =  blacklists.email AND b2.status = 0)");
                            $returnData['blacklist']= $BlacklistQuery->get();
                        }
                        break;
                    case 'blacklist':
                        $BlacklistQuery = Blacklist::selectRaw('type, email, rules, user_id');
                        $BlacklistQuery->where("status", 2);
                        $BlacklistQuery->whereRaw("email NOT IN (SELECT email FROM blacklists b2 WHERE b2.type = blacklists.type AND b2.email =  blacklists.email AND b2.status = 0)");
                        $returnData['blacklist']= $BlacklistQuery->get();
                        break;
                    default:
                        # code...
                        break;
                }
            }
        }

        // Update last_cli_update
        $ServerUpdate= ['last_cli_update' => time()];
        $Server = Server::where("hostname", $hostname)
            ->update($ServerUpdate);
        return response($returnData, 200);
    }
}
