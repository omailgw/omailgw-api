<?php

namespace App\Http\Controllers;

use App\Models\SmtpdAuth;
use Illuminate\Http\Request;

class SmtpdAuthController extends Controller
{
    public static function random_str($length = 40)
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user_id = auth()->user()->id;
        return response(SmtpdAuth::where("user_id", $user_id)->get(), 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $user_id = auth()->user()->id;
        $SmtpAuthData = $request->validate([
            'username' => ['required', 'string', 'min:4', 'max:40', 'regex:/^[a-zA-Z0-9]([a-zA-Z0-9._+@-]){2,40}[a-zA-Z0-9]$/i'],
            'description' => ['string', 'max:255']
        ]);
        $SmtpdAuthCount = SmtpdAuth::where('username', $SmtpAuthData['username'])->count();
        if ($SmtpdAuthCount == 0) {
            $SmtpAuth = SmtpdAuth::create([
                "username" => $SmtpAuthData['username'],
                'password' => $this->random_str(),
                "description" => $SmtpAuthData['description'],
                "user_id" => $user_id
            ]);
            return response($SmtpAuth, 201);
        } else {
            return response(["Message" => "Username already exist"], 400);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(SmtpdAuth $smtpdAuth)
    {
        $user_id = auth()->user()->id;
        $username = request()->route('username');
        $SmtpdAuth = SmtpdAuth::where("username", $username)
                            ->where("user_id", $user_id)
                            ->first();
        if ($SmtpdAuth) {
            return response($SmtpdAuth, 200);
        } else {
            return response(["Message" => "Not found"], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(SmtpdAuth $smtpdAuth)
    {
        $user_id = auth()->user()->id;
        $username = request()->route('username');
        $SmtpdAuthDelete = SmtpdAuth::where("username", $username)
                                ->where("user_id", $user_id)
                                ->delete();
        if ($SmtpdAuthDelete == 0) {
            return response(["Message" => "Not found"], 404);
        } else {
            return response(["Message" => "Ok"], 200);
        }
    }
}
