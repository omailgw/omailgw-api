<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Log;
use App\Models\Server;

class SpoolController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $auth = auth()->user();
        $hostname = request()->route('hostname');
        if ($hostname != 'all') {
            $Server = Server::where("hostname", $hostname)->first();
            if ($Server == null) {
                return response(["Message" => "Server not found"], 404);
            }
        }
        $Spool = DB::table('spools')
            ->select("hostname", "queueId", "size", "date", "from", "to", "msg", "spools.created_at", "spools.updated_at");

        // Si rôle "root" on affiche tout, sinon on applique les permissions des logs
        if ($auth->role != "root") {
            $Spool->orWhereIn('queueId', function($query) {
                $query->select('smtp_queueId')->from('log_smtps');
                $addLogPermission = New Log;
                $addLogPermission->addLogPermission($query, 'log_smtps');
                $query->distinct();
            });
        }
        $Spool->join('servers', 'servers.id', '=', 'spools.server_id');
        if ($hostname != 'all') {
            $Spool->where('server_id', $Server['id']);
        }
        return response($Spool->get(), 200);
    }

}
