<?php

namespace App\Http\Controllers;

use App\Models\Transport;
use App\Models\Server;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log as LaravelLog;



class TransportController extends Controller
{

    protected $loopOrigin = "";
    protected $loopOriginProtected = false;

    private function setLoopOrigin($adress)
    {
        if (!$this->loopOriginProtected) {
            LaravelLog::debug("Nouveau test boucle depuis " . $adress);
            $this->loopOrigin = $adress;
            $this->loopOriginProtected = true;
        }
    }

    private function resetLoopOrigin()
    {
        $this->loopOrigin = "";
        $this->loopOriginProtected = false;
    }

    /*
    * Validate a new transport route only if it doesnt create any loop
    */
    private function checkTransportLoop($serverAdress, $search, $nexthop)
    {
        if ($serverAdress == $nexthop) {
            LaravelLog::warning("Boucle auto-référence " . $serverAdress . " (search " . $search . ") vers" . $nexthop . " ! ");
            return false;
        }
        LaravelLog::debug("Verification de boucle sur server= " . $serverAdress . " search= " . $search . " next= " . $nexthop);
        $this->setLoopOrigin($serverAdress);
        // look for all destinations from current server that have same 'search' rule
        $comparedTransports = DB::table('transports')
            ->select('nexthop')
            ->leftJoin('servers', 'transports.server_id', '=', 'servers.id')
            ->where(DB::raw("CONCAT(servers.hostname, '.', servers.domain)"), $nexthop)
            ->where('transports.search', $search)
            ->get()->toArray();
        // compare those destination to route's origin

        foreach ($comparedTransports as $comparison) {
            $newNextHop = $comparison->nexthop;
            // formater l'adresse serveur cible
            $newNextHop = explode(":", $newNextHop)[0];
            $newNextHop = $newNextHop . (str_ends_with($newNextHop, '.') ? "" : ".");
            $newNextHop = substr($newNextHop, 0, -1);
            LaravelLog::debug("Verification en cours server= " . $nexthop . " next= " . $newNextHop);
            if ($newNextHop == $this->loopOrigin) {
                //found a loop, return false
                LaravelLog::warning("Boucle depuis " . $this->loopOrigin . " (search " . $search . ") ! Derniere route checkee : " . $serverAdress . "vers" . $nexthop);
                $this->resetLoopOrigin();
                return false;
            } else if (!$this->checkTransportLoop($nexthop, $search, $newNextHop)) {
                //as we continue checking deeper down the routes, any 'false' shall also be return higher up;
                return false;
            }
        }
        //finished checking this level deep, go back up with true = 'no loop found, you may continue'
        $this->resetLoopOrigin();
        return true;
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $hostname = request()->route('hostname');
        if ($hostname != 'all') {
            $Server = Server::where("hostname", $hostname)->first();
            if ($Server == null) {
                return response(["Message" => "Server not found"], 404);
            }
        }
        $Transport = Transport::select('transports.id as id', 'hostname', 'domain', 'search', 'search_regex', 'transport', 'nexthop', 'description', 'transports.created_at', 'users.id as user_id', 'email');
        if ($hostname != 'all') {
            $Transport->where('server_id', $Server['id']);
        }
        $Transport->join('servers', 'servers.id', '=', 'transports.server_id');
        $Transport->leftJoin('users', 'users.id', '=', 'transports.user_id');
        $Transport->orderBy('hostname', 'ASC');
        return response($Transport->get(), 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $hostname = request()->route('hostname');
        $user_id = auth()->user()->id;
        // Vérification de l'existance du serveur
        $server = Server::where('hostname', $hostname)->first();
        if ($server == null) {
            return response(['message' => "Server not found"], 404);
        }
        $TransportData = $request->validate([
            'search' => ['required', 'string', 'min:4', 'max:200'],
            'search_regex' => ['boolean'],
            'transport' => ['string'],
            'nexthop' => ['string'],
            'description' => []
        ]);
        // avant 1ere iteration de checkTransportLoop
        // formater l'adresse complète serveur actuel
        $serverAdress = $server->hostname . "." . $server->domain;
        $serverAdress = $serverAdress . (str_ends_with($serverAdress, '.') ? "" : ".");
        $serverAdress = substr($serverAdress, 0, -1);
        // formater la regle search de ce rerouting
        $search = $TransportData['search'];
        // formater l'adresse serveur cible
        $nexthop = explode(":", $TransportData['nexthop'])[0];
        $nexthop = $nexthop . (str_ends_with($nexthop, '.') ? "" : ".");
        $nexthop = substr($nexthop, 0, -1);
        if ($this->checkTransportLoop($serverAdress, $search, $nexthop)) {
            $TransportData['server_id'] = $server['id'];
            $TransportData['user_id'] = $user_id;
            // Enregistrement
            $Transport = Transport::create($TransportData);
            return response($Transport, 201);
        } else {
            return response(['message' => 'Loop in transport'], 409);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Transport $transport)
    {
        $hostname = request()->route('hostname');
        $Server = Server::where("hostname", $hostname)->first();
        if ($Server == null) {
            return response(["Message" => "Server not found"], 404);
        }
        $id = request()->route('id');
        $TransportDelete = Transport::where("id", $id)
            ->where('server_id', $Server['id'])
            ->delete();
        if ($TransportDelete == 0) {
            return response(["Message" => "Transport Not found"], 404);
        } else {
            return response(["Message" => "Ok"], 200);
        }
    }
}
