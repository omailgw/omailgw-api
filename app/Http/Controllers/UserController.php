<?php

namespace App\Http\Controllers;

use App\Models\Log;
use App\Models\LogPermission;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use DB;

class UserController extends Controller
{
    /**
     * Show user list
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $users = DB::table('users')
                    ->get();
        # @todo group_cancat avec log_permissions pour voir les permission...
        return response($users, 200);
    }
    /**
     * Store user (no password validation)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $userData = $request->validate([
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => ['required', 'string', 'min:8', 'max:30'],
            'role' => ['string', 'max:6']
        ]);
        $users = User::create([
            "email" => $userData['email'],
            "password" => bcrypt($userData['password']),
            "role" => $userData['role'],
        ]);
        // Default log Permission
        LogPermission::create([
            "user_id" => $users['id'],
            "field" => "from",
            "content" => $userData['email'],
        ]);
        return response($users, 201);
    }
    /**
     * Registre user
     *    If e-mail is in "from" log
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request) {
        $userData = $request->validate([
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => ['required', 'string', 'min:8', 'max:30']
        ]);
        // Check if e-mail is in log database
        $UserInLog = Log::where("from", $userData['email'])->first();
        if (empty($UserInLog)) {
            return response(['message' => "Unknown e-mail in log"], 403);
        } else {
            $users = User::create([
                "email" => $userData['email'],
                "password" => bcrypt($userData['password']),
            ]);
            // Default log Permission
            LogPermission::create([
                "user_id" => $users['id'],
                "field" => "from",
                "content" => $userData['email'],
            ]);
            return response($users, 201);
        }
    }
    /**
     * Login user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request) {
        $userData = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required', 'string', 'min:8', 'max:30']
        ]);

        $user = User::where("email", $userData['email'])->first();
        if (empty($user)) {
            return response(['message' => "Login incorrect"], 401);
        } elseif (!Hash::check($userData['password'], $user->password)) {
            return response(['message' => "Mot de passe incorrect"], 401);
        } else {
            $token = $user->createToken("token")->plainTextToken;
            return response( [
                #"user" => $user,
                "token" => $token,
                'role' => $user->role
            ], 201);
        }

    }
    /**
     * Logout user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request) {
        auth()->user()->tokens->each(function($token, $key){
            $token->delete();
        });
        return response(["message" => "Ok"], 200);
    }
    /**
     * Delete user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request) {
        $id = request()->route('id');
        $users = User::where("id", $id)->first();
        if (empty($users)) {
            return response(['message' => "Unknown user"], 401);
        } else {
            User::destroy($id);
            return response(["Message" => "User deleted"], 200);
        }

    }

    /**
     * Update user information
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {
        $id = request()->route('id');
        // venant de /admin/user/id)
        if (isset($id)) {
            $userData = $request->validate([
                'email' => ['email'],
                'password' => ['string', 'min:8', 'max:30'],
                'role' => ['string', 'max:6'],
                'rapport' => ['integer', 'min:0', 'max:3'],
                'rapport_verbose' => ['integer', 'min:0', 'max:1']
            ]);
        // Sinon c'est /user/me
        } else {
            $id = auth()->user()->id;
            $userData = $request->validate([
                'email' => ['email'],
                'password' => ['string', 'min:8', 'max:30'],
                'rapport' => ['integer', 'min:0', 'max:3'],
                'rapport_verbose' => ['integer', 'min:0', 'max:1']
            ]);
        }
        if(isset($userData["password"])) {
            $userData["password"] = bcrypt($userData['password']);
        }
        $user = User::where("id", $id)
            ->update($userData);
        if ($user == 1) {
            return response(["message" => "Ok"], 200);
        } else {
            return response(["message" => "Unknow user id"], 404);
        }
    }
    /**
     * Update personal user password (Auth id)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateMePassword(Request $request) {
        $id = auth()->user()->id;
        $userData = $request->validate([
            'old_password' => ['required', 'string', 'min:8', 'max:30'],
            'new_password' => ['required', 'string', 'min:8', 'max:30']
        ]);

        # Vérification de l'ancien mot de passe
        $users = User::where("id", $id)->first();
        if (!Hash::check($userData['old_password'], $users->password)) {
            return response(['message' => "Incorrect old password"], 401);
        }

        # Mise à jour du mot de passe
        $users = User::where("id", $id)
                        ->update([
                            "password" => bcrypt($userData['new_password']),
                        ]);
        $this->logout($request);
        return response(["message" => "Ok"], 200);
    }
    /**
     * Show user information (specific ID)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request) {
        // By request ID or auth ID
        $id = request()->route('id');
        if (empty($id)) {
            $id = auth()->user()->id;
        }
        $users = User::where("id", $id)->first();
        if (empty($users)) {
            return response(['message' => "Unknown user"], 401);
        } else {
            return response($users, 200);
        }
    }
}
