<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckRole
{
    public function handle(Request $request, Closure $next, ...$rolesDemandes)
    {
        $user = auth()->user();
        if (!$user) {
            return response()->json(['message' => 'Unauthorized'], 401);
        } else if (!in_array($user->role, $rolesDemandes)) {
            return response()->json(['message' => 'Forbidden'], 403);
        } else {
            return $next($request);
        }
    }
}
