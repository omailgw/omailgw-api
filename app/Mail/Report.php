<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Report extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $logStat;
    public $requestData;
    /**
     * Create a new message instance.
     *
     * @param \App\Models\User  $user
     * @param \App\Models\LogStat  $logStat
     * @param $requestData
     * @return void
     */
    public function __construct($user, $logStat, $requestData)
    {
        $this->user = $user;
        $this->logStat = $logStat;
        $this->requestData = $requestData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Report oMailgw')->view('emails.report');
    }
}
