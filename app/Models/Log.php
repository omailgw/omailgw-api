<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\SmtpdAuth;

class Log extends Model
{
    // Pour éviter "has no column named updated_at"
    public $timestamps = false;

    use HasFactory;

    protected $fillable = [
        'server',
        'queueId',
        'from',
        'from_dom',
        'to',
        'to_dom',
        'smtpd_date',
        'smtpd_date_day',
        'smtpd_client',
        'smtpd_username',
        'smtpd_codeError',
        'smtpd_msg',
        'message_id',
        'cleanup',
        'qmgr_size',
        'qmgr_nrcpt',
        'qmgr_msg',
        'msg',
        'smtp_queueId',
        'smtp_to',
        'smtp_date',
        'smtp_relay',
        'smtp_status',
        'smtp_msg'
    ];

    /**
     * Add log permission in query
     *
     * @param  <mixed> $requestData
     * @param  <mixed> $table : logs|log_smtps|join (logs = default)
     */
    public function addLogPermission($logQuery, $table = 'logs', $user_id = null, $user_role = null)
    {
        // Si aucun paramètre n'est passé, on est authentifié, on se sert de ça
        if (auth()->user() != null) {
            $user_id = auth()->user()->id;
            $user_role = auth()->user()->role;
        }
        // Si rôle "root" on affiche tout (donc aucune permission appliqué)
        if ($user_role != "root") {
            $logQuery->orWhere(function($logQuery) use ($user_id,$table){
                LogPermission::where("user_id", $user_id)->each(function($data) use ($logQuery, $table) {
                    $logQuery->orWhere($table.'.'.$data['field'], $data['content']);
                });
                SmtpdAuth::where("user_id", $user_id)->each(function($data) use ($logQuery, $table) {
                    if ($table == "join" || $table == "logs") {
                        $logQuery->orWhere('logs.smtpd_username', $data['username']);
                    }
                    if ($table == "join" || $table == "log_smtps") {
                        $logQuery->orWhere('log_smtps.smtpd_username', $data['username']);
                    }
                });
            });
        }
        // Si c'est un user (pas admin, pas root) on affiche pas les rebond
        if ($user_role == "user" ) {
            // Ne pas afficher les rebond
            $logQuery->where($table.'.rebound', 0);
        }
    }


    /**
     * Add log type (select SMTP error in input or output)
     *
     * @param  \DataTables
     * @param  $type
     */
    public function addLogType($logQuery, $type)
    {
        switch ($type) {
            case 'errorOut':
                $logQuery->where('smtp_status', '!=', 'sent');
                break;
            case 'errorIn':
                $logQuery->whereRaw('(`qmgr_msg` != \'queue active\' OR `smtpd_codeError` IS NOT NULL OR `cleanup` IS NOT NULL)');
                break;
            default:
                exit("addLogType : type error");
        }
    }

    /**
     * Log statistiques
     *
     * @param  <mixed> $requestData
     * @param  <mixed> $user (only if not authentificated)
     * @return \Illuminate\Http\Response
     */
    public function stat($requestData, $user_id = null, $user_role = null)
    {
        // Début du debug si mon app est en mode debug
        if (config('app.debug'))
            DB::connection()->enableQueryLog();
        // Construction de la requête commune
        $logQuery = DB::table('logs');
        $this->addLogPermission($logQuery, 'logs', $user_id, $user_role);
        // Construction de la requête log_smtps
        $logQueryLogSmtps = DB::table('log_smtps');
        $this->addLogPermission($logQueryLogSmtps, 'log_smtps', $user_id, $user_role);
        // Bornage de date
        $logQuery->where('smtpd_date', ">", $requestData['dateStart']);
        $logQueryLogSmtps->where('smtp_date', ">", $requestData['dateStart']);
        $logQuery->where('smtpd_date', "<", $requestData['dateEnd']);
        $logQueryLogSmtps->where('smtp_date', "<", $requestData['dateEnd']);
        // La limit est ajouté plus loin

        // ----- Une requête par demande
        // Total erreur en sortie
        if (in_array('countOutError', $requestData['type'])) {
            $countOutError = clone $logQueryLogSmtps;
            $this->addLogType($countOutError, 'errorOut');
            $data['countOutError'] = $countOutError->count();
        }
        // Total erreur en entrée
        if (in_array('countInError', $requestData['type'])) {
            $countInError = clone $logQuery;
            $this->addLogType($countInError, 'errorIn');
            $data['countInError'] = $countInError->count();
        }
        // Total
        if (in_array('countTotal', $requestData['type'])) {
            if (empty($data['countInError'])) {
                $countInError = clone $logQuery;
                $this->addLogType($countInError, 'errorIn');
            }
            $countTotal = clone $logQueryLogSmtps;
            $data['countTotal'] = $countTotal->count()+$countInError->count();
        }
        // Total par jour
        if (in_array('getByDay', $requestData['type'])) {
            // Init
            $dateNext = $requestData['dateStart'];
            while ($dateNext <= $requestData['dateEnd']) {
                $dateYMD = date('Y-m-d', $dateNext);
                $data['getByDay'][$dateYMD]['total'] = 0;
                $data['getByDay'][$dateYMD]['errorIn'] = 0;
                $data['getByDay'][$dateYMD]['errorOut'] = 0;
                // Ajout d'une journée
                $dateNext = $dateNext+86400;
            }
            // Total en erreur sur l'entrée
            $getByDayInError = clone $logQuery;
            $getByDayInError->select(DB::raw('count(queueId) nb, smtpd_date_day as dateYMD'));
            $this->addLogType($getByDayInError, 'errorIn');
            $getByDayInError->groupBy('dateYMD');
            foreach($getByDayInError->get() as $getByDayInError) {
                if (isset($data['getByDay'][$getByDayInError->dateYMD]['errorIn'])) {
                    $data['getByDay'][$getByDayInError->dateYMD]['errorIn'] = $getByDayInError->nb;
                }
            }
            // Total en erreur sur la sortie
            $getByDayOutError = clone $logQueryLogSmtps;
            $getByDayOutError->select(DB::raw('count(smtp_queueId) nb, smtp_date_day as dateYMD'));
            $this->addLogType($getByDayOutError, 'errorOut');
            $getByDayOutError->groupBy('dateYMD');
            foreach($getByDayOutError->get() as $getByDayOutError) {
                if (isset($data['getByDay'][$getByDayOutError->dateYMD]['errorOut'])) {
                    $data['getByDay'][$getByDayOutError->dateYMD]['errorOut'] = $getByDayOutError->nb;
                }
            }
            // Total
            $getByDay = clone $logQueryLogSmtps;
            $getByDay->select(DB::raw('count(smtp_queueId) nb, smtp_date_day as dateYMD'));
            $getByDay->groupBy('dateYMD');
            foreach($getByDay->get() as $getByDay) {
                if (isset($data['getByDay'][$getByDay->dateYMD]['total'])) {
                    $data['getByDay'][$getByDay->dateYMD]['total'] = $getByDay->nb+$data['getByDay'][$getByDay->dateYMD]['errorIn'];
                }
            }
        }

        // Limit (pour les TOP uniquement, pour les getByDay ça fait des valeurs à 0 sinon..)
        if (isset($requestData['limit'])) {
            $logQuery->limit($requestData['limit']);
            $logQueryLogSmtps->limit($requestData['limit']);
        } else {
            $logQuery->limit(10);
            $logQueryLogSmtps->limit(10);
        }

        // Top des erreur de sortie
        if (in_array('topOutError', $requestData['type'])) {
            $topOutError = clone $logQueryLogSmtps;
            $topOutError->select(DB::raw('COUNT(smtp_queueId) nb, smtp_status, smtp_msg'));
            $this->addLogType($topOutError, 'errorOut');
            $topOutError->groupBy('smtp_msg', 'smtp_status');
            $topOutError->orderBy('nb', 'DESC');
            $data['topOutError'] = $topOutError->get();
        }
        // Top des erreur en entrée
        if (in_array('topInError', $requestData['type'])) {
            $topInError = clone $logQuery;
            $topInError->select(DB::raw('COUNT(logs.queueId) nb, cleanup, smtpd_codeError, smtpd_msg'));
            $this->addLogType($topInError, 'errorIn');
            $topInError->groupBy('cleanup', 'smtpd_codeError', 'smtpd_msg');
            $topInError->orderBy('nb', 'DESC');
            $data['topInError'] = $topInError->get();
        }
        if (in_array('topInDomain', $requestData['type'])) {
            $topInDomain = clone $logQuery;
            $topInDomain->select(DB::raw('count(`from_dom`) nb, `from_dom` dom'));
            $topInDomain->groupBy('dom');
            $topInDomain->orderBy('nb', 'DESC');
            $data['topInDomain'] = $topInDomain->get();
        }
        if (in_array('topOutDomain', $requestData['type'])) {
            $topOutDomain = clone $logQueryLogSmtps;
            $topOutDomain->select(DB::raw('count(`smtp_to_dom`) nb, smtp_to_dom dom'));
            $topOutDomain->groupBy('dom');
            $topOutDomain->orderBy('nb', 'DESC');
            $data['topOutDomain'] = $topOutDomain->get();
        }

        if (in_array('topInDomainError', $requestData['type'])) {
            $topInDomainError = clone $logQuery;
            $topInDomainError->select(DB::raw('count(`from_dom`) nb, `from_dom` dom'));
            $topInDomainError->groupBy('dom');
            $this->addLogType($topInDomainError, 'errorIn');
            $topInDomainError->orderBy('nb', 'DESC');
            $data['topInDomainError'] = $topInDomainError->get();
        }
        if (in_array('topOutDomainError', $requestData['type'])) {
            $topOutDomainError = clone $logQueryLogSmtps;
            $topOutDomainError->select(DB::raw('count(`smtp_to_dom`) nb, smtp_to_dom dom'));
            $this->addLogType($topOutDomainError, 'errorOut');
            $topOutDomainError->groupBy('dom');
            $topOutDomainError->orderBy('nb', 'DESC');
            $data['topOutDomainError'] = $topOutDomainError->get();
        }
        if (in_array('topInEmail', $requestData['type'])) {
            $topInEmail = clone $logQuery;
            $topInEmail->select(DB::raw('count(`from`) nb, `from`'));
            $topInEmail->groupBy('from');
            $topInEmail->orderBy('nb', 'DESC');
            $data['topInEmail'] = $topInEmail->get();
        }
        if (in_array('topOutEmail', $requestData['type'])) {
            $topOutEmail = clone $logQueryLogSmtps;
            $topOutEmail->select(DB::raw('count(`smtp_to`) nb, `smtp_to` `to`'));
            $topOutEmail->groupBy('smtp_to');
            $topOutEmail->orderBy('nb', 'DESC');
            $data['topOutEmail'] = $topOutEmail->get();
        }
        if (in_array('topInEmailError', $requestData['type'])) {
            $topInEmailError = clone $logQuery;
            $topInEmailError->select(DB::raw('count(`from`) nb, `from`'));
            $this->addLogType($topInEmailError, 'errorIn');
            $topInEmailError->groupBy('from');
            $topInEmailError->orderBy('nb', 'DESC');
            $data['topInEmailError'] = $topInEmailError->get();
        }
        if (in_array('topOutEmailError', $requestData['type'])) {
            $topOutEmailError = clone $logQueryLogSmtps;
            $topOutEmailError->select(DB::raw('count(`smtp_to`) nb, `smtp_to` `to`'));
            $this->addLogType($topOutEmailError, 'errorOut');
            $topOutEmailError->groupBy('smtp_to');
            $topOutEmailError->orderBy('nb', 'DESC');
            //dd($topOutEmailError->toSql());
            $data['topOutEmailError'] = $topOutEmailError->get();
        }
        // Ajout du debug laravel si mon app est en mode debug
        if (config('app.debug')) {
            $queries = DB::getQueryLog();
            $data['debug']=$queries;
        }
        return $data;
    }
}
