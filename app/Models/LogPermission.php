<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogPermission extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'field',
        'content'
    ];

}
