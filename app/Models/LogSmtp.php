<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogSmtp extends Model
{
    // Pour éviter "has no column named updated_at"
    public $timestamps = false;

    use HasFactory;

    protected $fillable = [
        'smtp_queueId',
        'server',
        'smtpd_username',
        'from',
        'from_dom',
        'smtp_to',
        'smtp_to_dom',
        'smtp_date',
        'smtp_date_day',
        'smtp_relay',
        'smtp_status',
        'smtp_msg'
    ];
}
