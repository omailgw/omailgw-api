<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Spool extends Model
{
    use HasFactory;
    protected $fillable = [
        'queueId',
        'server_id',
        'size',
        'date',
        'from',
        'to',
        'msg'
    ];
}
