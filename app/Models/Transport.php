<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transport extends Model
{
    use HasFactory;
    protected $fillable = [
        'server_id',
        'search',
        'search_regex',
        'transport',
        'nexthop',
        'description',
        'user_id'
    ];
}
