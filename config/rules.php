<?php

return [
	# A appliquer sur le champs "to"
	'BlacklistRuleTo' => [
		'UserUnknown' => [
			'nb' => '2',
			'columns' =>[
				'smtp_status' => [
					'!=', 
					[
						'sent',
					],
				],
				'smtp_msg'=>[ 
					'like', 
					[
						'%Recipient address rejected: this mailbox is inactive and has been disabled (in reply to RCPT TO command)',
						'%Recipient address rejected: unverified address%(in reply to RCPT TO command)',
						'%Requested mail action aborted, mailbox not found%',
						'%rejected: user unknown (in reply to RCPT TO command)',
						'%Recipient address rejected: User unknown%(in reply to RCPT TO command)',
						'%Requested action not taken: mailbox unavailable%(in reply to RCPT TO command)',
						'%cannot be delivered. This mailbox is disabled %(in reply to end of DATA command)',
						'This mailbox has been blocked due to inactivity%(in reply to RCPT TO command)',
						'%550-5.1.1 The email account that you tried to reach does not exist%',
						'%550 5.1.1 user unknown (UserSearch)%',
						'%Adresse d au moins un destinataire invalide%',
					],
				],
			],
		],
	],
	# A appliquer sur le champs "from"
	'BlacklistRuleFrom' => [
		
	]
]

?>