<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Log>
 */
class LogFactory extends Factory
{
    /**
     * Generate random string
     *
     * @return string
     */
    public function generateRandomString($length = 8) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function generateFrom() {
        $from = array(
            "user@retzo.net",
            "michel@retzo.net",
            "dupond@retzo.net",
            "guillaume@retzo.net",
            "contact@domain.com",
            "guillaume@client.fr",
            "jean@client.com",
            "mailingliste-owner@retzo.net",
            "mailingliste-owner@domain.com",
        );
        return $from[array_rand($from)];
    }


    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'server' => "mailgw1",
            'queueId' => "RAND".$this->generateRandomString(),
            'smtpd_date' => mt_rand(time()-604800,time()),
            'from' => $this->generateFrom(),
            //'to' => $this->faker->safeEmail()
            'smtpd_client' => "myserver.mydomain.tdl[x.x.x.x]",
            'smtpd_username' => "srv1@retzo.net",
            "qmgr_size" => mt_rand(1000,5000),
            "qmgr_nrcpt" => mt_rand(1,30),
            "qmgr_msg" => "queue active",
        ];
    }
}
