<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password', 60);
            $table->tinyInteger('rapport')->default(0)->comment('0=Aucun 1=quotidien 2=hebdo 3=mensuel ');
            $table->tinyInteger('rapport_verbose')->default(0)->comment('0=juste les erreurs 1=tout ');
            $table->string('role',6)->default('user')->comment('possibles : root, admin, user, mailgw');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
