<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_permissions', function (Blueprint $table) {
            $table->id();
            $table->enum('field', ['from', 'from_dom', 'smtpd_username', 'server']);
            $table->string("content")->nullable(false);
            $table->foreignId('user_id')
                ->nullable(false)
                ->constrained('users')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_permissions');
    }
}
