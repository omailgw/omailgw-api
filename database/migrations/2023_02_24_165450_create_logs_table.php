<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->id();
            $table->boolean("rebound")->default(false);;
            $table->string("server", 50);
            $table->string("queueId", 15)->unique();
            $table->string("from", 255)->nullable();
            $table->string("from_dom", 255)->storedAs("SUBSTR(`from`, (INSTR(`from`, '@')+1), (LENGTH(`from`)-INSTR(`from`, '@')))");
            $table->string("to", 255)->nullable();
            $table->string("to_dom", 255)->storedAs("SUBSTR(`to`, (INSTR(`to`, '@')+1), (LENGTH(`to`)-INSTR(`to`, '@')))");
            $table->unsignedInteger("smtpd_date")->nullable();
            $table->date("smtpd_date_day")->nullable();
            $table->string("smtpd_client", 100)->nullable();
            $table->string("smtpd_username", 31)->nullable();
            $table->string("smtpd_codeError", 10)->nullable();
            $table->string("smtpd_msg", 255)->nullable();
            $table->string("message_id", 360)->nullable();
            $table->string("cleanup", 255)->nullable();
            $table->unsignedInteger("qmgr_size")->nullable();
            $table->unsignedInteger("qmgr_nrcpt")->nullable();
            $table->string("qmgr_msg", 31)->nullable();
            $table->text("msg")->nullable();
            // @todo : finir les index
            $table->index(["smtpd_date"]);
            $table->index(["from"]);
            $table->index(["from_dom"]);
            $table->engine = "MYISAM";
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
