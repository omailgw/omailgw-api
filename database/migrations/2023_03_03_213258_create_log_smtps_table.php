<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */

     public function up(): void
     {
         Schema::create('log_smtps', function (Blueprint $table) {
            $table->id();
            $table->boolean("rebound")->default(false);;
            $table->string("smtp_queueId", 20);
            $table->string("server", 50)->nullable();
            $table->string("smtpd_username", 31)->nullable();
            $table->string("from", 255)->nullable();
            $table->string("from_dom", 255)->storedAs("SUBSTR(`from`, (INSTR(`from`, '@')+1), (LENGTH(`from`)-INSTR(`from`, '@')))");
            $table->string("smtp_to", 255)->nullable();
            $table->string("smtp_to_dom", 255)->storedAs("SUBSTR(`smtp_to`, (INSTR(`smtp_to`, '@')+1), (LENGTH(`smtp_to`)-INSTR(`smtp_to`, '@')))");
            $table->unsignedInteger("smtp_date")->nullable();
            $table->date("smtp_date_day")->nullable();
            $table->string("smtp_relay", 127)->nullable();
            $table->string("smtp_status", 10)->nullable();
            $table->text("smtp_msg")->nullable();
            // @todo : finir les index
            $table->index(["smtp_queueId"]);
            $table->index(["smtp_date"]);
            $table->index(["from"]);
            $table->index(["from_dom"]);
            $table->index(["smtp_to_dom"]);
            $table->engine = "MYISAM";
         });
     }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('log_smtps');
    }
};
