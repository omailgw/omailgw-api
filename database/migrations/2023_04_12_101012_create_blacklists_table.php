<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('blacklists', function (Blueprint $table) {
            $table->id();
            $table->string('email', 255);
            $table->unsignedInteger('status'); // 0 = Désactivé par un utilisateur (plus d'effet - sorte de withiliste) 1 = Non bloqué mais count en cours... 2 = Seuil de la rules dépassé : bloqué
            $table->unsignedInteger('type'); // 1 = from 2 = to
            $table->string('rules', 15)->nullable();
            $table->unsignedInteger('rules_count')->default(0);
            $table->foreignId('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('blacklists');
    }
};
