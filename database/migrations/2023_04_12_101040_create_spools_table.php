<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('spools', function (Blueprint $table) {
            $table->string("queueId", 15)->unique();
            $table->foreignId('server_id')
                ->nullable(false)
                ->constrained('servers')
                ->onDelete('cascade');
            $table->unsignedInteger("size");
            $table->unsignedInteger("date");
            $table->string("from", 255);
            $table->string("to", 255);
            $table->string("msg", 127);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('spools');
    }
};
