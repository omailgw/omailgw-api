<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transports', function (Blueprint $table) {
            $table->id();
            $table->foreignId('server_id')
                ->nullable(false)
                ->constrained('servers')
                ->onDelete('cascade');
            $table->string("search", 255);
            $table->boolean("search_regex")->default(false);
            $table->string("transport", 10)->default("smtp");
            $table->string('nexthop', 255)->nullable();
            $table->string('description', 255)->nullable();
            $table->foreignId('user_id')
                ->nullable()
                ->constrained('users');
            $table->timestamps();
            $table->unique(['server_id','search', 'nexthop']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transports');
    }
};
