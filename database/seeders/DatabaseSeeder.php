<?php

namespace Database\Seeders;


use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Log;
use App\Models\LogPermission;
use App\Models\Server;
use App\Models\SmtpdAuth;
use App\Models\Transport;
use App\Models\Blacklist;
use App\Models\LogSmtp;
use App\Models\Spool;
use DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (\App::environment(['local', 'staging'])) {

            // ------- USER
            // Create users
            $users = [
                [
                    'email' => 'root@retzo.net',
                    'password' => bcrypt('password'),
                    'role' => 'root'
                ],
                [
                    'email' => 'admin@retzo.net',
                    'password' => bcrypt('password'),
                    'role' => 'admin'
                ],
                [
                    'email' => 'user@retzo.net',
                    'password' => bcrypt('password'),
                    'role' => 'user'
                ]
            ];

            // Insert users in a single query
            User::insert($users);

            // ------- PERMISSION
            // user@retzo.net
            $UserUser = User::where("email", "user@retzo.net")->first();
            LogPermission::create([
                "user_id" => $UserUser['id'],
                "field" => "from",
                "content" => 'user@retzo.net',
            ]);
            // admin@retzo.net
            $UserAdmin = User::where("email", "admin@retzo.net")->first();
            LogPermission::create([
                "user_id" => $UserAdmin['id'],
                "field" => "from_dom",
                "content" => 'retzo.net',
            ]);

            // ------- SERVER
            // Create servers for each user
            Server::create([
                "user_id" => 1,
                "hostname" => "mailgw1",
                "domain" => "retzo.net"
            ]);
            Server::create([
                "user_id" => 1,
                "hostname" => "mailgw2",
                "domain" => "retzo.net"
            ]);

            // ------- SPOOL
            Log::create([
                'server' => "mailgw1",
                'queueId' => "RAND00001",
                'smtpd_date' => time(),
                'from' => "jean@retzo.net",
                'smtpd_client' => "myserver.mydomain.tdl[x.x.x.x]",
                'smtpd_username' => "srv1@retzo.net",
                "qmgr_size" => 1211,
                "qmgr_nrcpt" => mt_rand(1,30),
                "qmgr_msg" => "queue active"
            ]);
            LogSmtp::create([
                'server' => "mailgw1",
                'smtp_queueId' => "RAND00001",
                'smtp_date' => time(),
                'from' => "jean@retzo.net",
                'smtpd_username' => "srv1@retzo.net",
                "smtp_to" => "michel@gmail.fr",
                "smtp_relay" => "none",
                "smtp_status" => "deferred",
                "smtp_msg" => "delivery temporarily suspended: connect to gmail.fr"
            ]);
            Spool::create([
                'queueId' => "RAND00001",
                'server_id' => 1,
                'size' => 1211,
                'date' => time(),
                'from' => 'jean@retzo.net',
                'to' => 'michel@gmail.fr',
                'msg' => 'delivery temporarily suspended: connect to gmail.fr',
            ]);

            // ------- BLACKLIST
            Blacklist::create([
                "email" => "toto@gmail.fr",
                "status" => 2,
                "type" => 2,
                "rules" => "UserUnknown",
                "rules_count" => 3
            ]);
            Blacklist::create([
                "email" => "michel@impot.gouv.fr",
                "status" => 0,
                "type" => 2,
            ]);
            Blacklist::create([
                "email" => "michel@gmail.fr",
                "status" => 1,
                "type" => 2,
                "rules" => "UserUnknown",
                "rules_count" => 1
            ]);

            // ------- AUTH SMTP
            SmtpdAuth::create([
                "user_id" => $UserAdmin['id'],
                "username" => "srv1@retzo.net",
                "password" => "D0SVigXvUtswKB45jEhGmEAl5CSkXotgB3Zyw3GR",
                "description" => "E-mail server master"
            ]);

            // ------- TRANSPORT$
            Transport::insert([
                [
                    "server_id" => 1,
                    "search" => "/yahoo(.[a-z]{2,3}){1,2}$/",
                    "search_regex" => 1,
                    "transport" => "smtp",
                    "nexthop" => "mailgw2.retzo.net",
                    "description" => "Yahoo vers mailgw2",
                    "user_id" => 1,
                ],
                [
                    "server_id" => 1,
                    "search" => "retzo.net",
                    "search_regex" => 0,
                    "transport" => "smtp",
                    "nexthop" => "mailgw2.retzo.net",
                    "description" => "Retzo vers mailgw2",
                    "user_id" => 1,
                ]
            ]);

            // Generate logs only in local or staging environment
            Log::factory(100)->create();

            DB::update('update logs set smtpd_date_day = CAST(FROM_UNIXTIME(smtpd_date) AS DATE)');

            // LOG SMTP
            $Logs = Log::select();
            foreach ($Logs->get() as $Log) {
                LogSmtp::create([
                    "smtp_queueId" => $Log['queueId'],
                    "server" => $Log['server'],
                    "smtpd_username" => $Log['smtpd_username'],
                    "from" => $Log['from'],
                    "smtp_to" => mt_rand(1111111111,9999999999).'@domain.com',
                    "smtp_date" => $Log['smtpd_date'],
                    "smtp_date_day" => $Log['smtpd_date_day'],
                    "smtp_relay" => "server.relay.fr",
                    "smtp_status" => "sent",
                    "smtp_msg" => "250 2.0.0 Ok: queued as RAND".mt_rand(1111111111,9999999999)
                ]);
            }


        } else {

            $users = [
                [
                    'email' => 'root@retzo.net',
                    'password' => bcrypt('password'),
                    'role' => 'root'
                ]
            ];
            // Insert users in a single query
            User::insert($users);

        }

    }
}
