La documentation complète de l'API au format OpenApi 3.0.0 : https://app.swaggerhub.com/apis/retzo.net/oMailgw/

Le schéma de base de donnée : https://drawsql.app/teams/david-54/diagrams/v0-9

 La documentation est à retrouver par ici  : https://framagit.org/omailgw/omailgw-doc