<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Rapport</title>
</head>
<body>
    <h1>Rapport e-mail</h1>
    <p>Rapport d'expédition d'e-mail sur l'infrastructure {{ config('app.url') }} entre le {{ date('d/m/Y H:i:s', $requestData['dateStart']) }} et le {{ date('d/m/Y  H:i:s', $requestData['dateEnd']) }}</p>
    @if ($user["rapport_verbose"] === 1)
        <h2>Statistiques de traitement sur la période</h2/>
        <ul>
            <li>Nombre d'e-mail traité  : {{ $logStat['countTotal'] }}</li>
            <li>Erreur en entrée : {{ $logStat['countInError'] }} ({{round(100*$logStat['countInError']/$logStat['countTotal'],2)}}%)</li>
            <li>Erreur en sortie : {{ $logStat['countOutError'] }} ({{round(100*$logStat['countOutError']/$logStat['countTotal'],2)}}%)</li>
        </ul>
    @endif

    @if ($user["rapport_verbose"] === 1 || $logStat['countOutError'] != 0)
        <h2>TOP domaines de sortie en erreur (<a href="{{ config('app.url') }}/logTable&type=errorOut">+</a>)</h2>
        <ul>
            @if (isset($logStat['topOutDomainError']))
                @foreach ($logStat['topOutDomainError'] as $topOutDomainError)
                    <li>{{ $topOutDomainError['nb'] }}		{{ $topOutDomainError['dom'] }}</li>
                @endforeach
            @endif
        </ul>

        <h2>TOP des erreur en sortie (<a href="{{ config('app.url') }}/logTable&type=errorOut">+</a>)</h2>
        <ul>
            @if (isset($logStat['topOutError']))
                @foreach ($logStat['topOutError'] as $topOutError)
                    <li>{{ $topOutError['nb'] }} -   {{ $topOutError['smtp_status'] }}    -   {{ $topOutError['smtp_msg'] }}</li>
                @endforeach
            @endif
        </ul>

    @endif

    @if ($user["rapport_verbose"] === 0 || $logStat['countInError'] != 0)
        <h2>TOP des erreur en entrée (<a href="{{ config('app.url') }}/logTable&type=errorIn">+</a>)</h2>
        <ul>
            @if (isset($logStat['topInError']))
                @foreach ($logStat['topInError'] as $topInError)
                    <li>{{ $topInError['nb'] }} -   {{ $topInError['cleanup'] }} -  {{ $topInError['smtpd_codeError'] }}    -   {{ $topInError['smtpd_msg'] }}</li>
                @endforeach
            @endif
        </ul>
    @endif
    @if ($user["rapport_verbose"] === 1)
        <h2>TOP des domaines destinataire (<a href="{{ config('app.url') }}">+</a>)</h2>
        <ul>
            @if (isset($logStat['topOutDomain']))
                @foreach ($logStat['topOutDomain'] as $topOutDomain)
                    <li>{{ $topOutDomain['nb'] }}		{{ $topOutDomain['dom'] }}</li>
                @endforeach
            @endif
        </ul>

        <h2>TOP des domaines émetteurs (<a href="{{ config('app.url') }}">+</a>)</h2>
        <ul>
            @if (isset($logStat['topInDomain']))
                @foreach ($logStat['topInDomain'] as $topInDomain)
                    <li>{{ $topInDomain['nb'] }}		{{ $topInDomain['dom'] }}</li>
                @endforeach
            @endif
        </ul>
    @endif
    Pour modifier les paramètres de réception de rapport, rendez vous sur : <a href="{{ config('app.url') }}/user/me">{{ config('app.url') }}/user/me</a>
</body>
</html>
