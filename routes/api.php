<?php

use App\Http\Controllers\BlacklistController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\LogPermissionController;
use App\Http\Controllers\ServerController;
use App\Http\Controllers\SmtpdAuthController;
use App\Http\Controllers\SpoolController;
use App\Http\Controllers\TransportController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Documentation : https://app.swaggerhub.com/apis/retzo.net/oMailgw/
|
*/

# USER : all
# Log in, get token : all
Route::post('/user/login', [UserController::class, "login"]);
# Register new user : all
Route::post('/user/register', [UserController::class, "register"]);

# Check we have a token
Route::middleware(['auth:sanctum', 'verified'])->group(function () {

    # USER
    # Log out, destroy token : all connected
    Route::post('/user/logout', [UserController::class, "logout"]);
    # Update user information : all connected
    Route::post('/user/me', [UserController::class, "update"]);
    # Show user information : all connected
    Route::get('/user/me', [UserController::class, "show"]);
    # Update user password : root, admin, user
    Route::post('/user/me/password', [UserController::class, "updateMePassword"])->middleware(['role:root,admin,user']);

    # SMTP AUTH
    # Show all smtp auth. access for user : root, admin
    Route::get('/user/smtp-auth/list', [SmtpdAuthController::class, "index"])->middleware(['role:root,admin']);
    # Add one smtp auth. access to user : root, admin
    Route::post('/user/smtp-auth/add', [SmtpdAuthController::class, "store"])->middleware(['role:root,admin']);
    # Show one smtp auth. access by username : root, admin
    Route::get('/user/smtp-auth/{username}', [SmtpdAuthController::class, "show"])->middleware(['role:root,admin']);
    # Delete one smtp auth. access by username : root, admin
    Route::delete('/user/smtp-auth/{username}', [SmtpdAuthController::class, "destroy"])->middleware(['role:root,admin']);

    # ADMIN
    # Create an admin : root
    Route::post('/admin/user/add', [UserController::class, "store"])->middleware(['role:root']);
    # Show all admins : root
    Route::get('/admin/user/list', [UserController::class, "index"])->middleware(['role:root']);
    # Show one admin by id : root
    Route::get('/admin/user/{id}', [UserController::class, "show"])->middleware(['role:root']);
    # Update admin infos by id : root
    Route::post('/admin/user/{id}', [UserController::class, "update"])->middleware(['role:root']);
    # Delete an admin by id : root
    Route::delete('/admin/user/{id}', [UserController::class, "delete"])->middleware(['role:root']);

    # Show all permissions for an admin by id : root
    Route::get('/admin/user/{id}/log-permission', [LogPermissionController::class, "show"])->middleware(['role:root']);
    # Update permissions for an admin by id : root
    Route::post('/admin/user/{id}/log-permission', [LogPermissionController::class, "store"])->middleware(['role:root']);
    # Delete an admin permissions by id : root
    Route::delete('/admin/user/{id}/log-permission', [LogPermissionController::class, "destroy"])->middleware(['role:root']);

    # LOGS
    # Search in log : root, admin, user
    Route::post('/log/search', [LogController::class, "search"])->middleware(['role:root,admin,user']);
    # Show log stats : root, admin, user
    Route::get('/log/stat', [LogController::class, "stat"])->middleware(['role:root,admin,user']);
    # Add log : root, mailgw
    Route::post('/log', [LogController::class, "store"])->middleware(['role:root,mailgw']);
    # For dataTables : root, admin, user
    Route::get('/log/dataTables', [LogController::class, "parseDataTables"])->middleware(['role:root,admin,user']);
    # Clean log : root
    Route::delete('/log/clean', [LogController::class, "clean"])->middleware(['role:root']);
    # Show log by queueId : root, admin, user
    Route::get('/log/{queueId}', [LogController::class, "show"])->middleware(['role:root,admin,user']);


    # BLACKLIST
    # Show the blacklisted emails : root, admin, user
    Route::get('/blacklist/list', [BlacklistController::class, "index"])->middleware(['role:root,admin,user']);
    # Add an email to the blacklist : root, admin
    Route::post('/blacklist/add', [BlacklistController::class, "store"])->middleware(['role:root,admin']);
    # Delete an email from the blacklist : root, admin, user
    Route::delete('/blacklist/{id}', [BlacklistController::class, "destroy"])->middleware(['role:root,admin']);

    # SERVER
    # Show servers list : root, admin
    Route::get('/server/list', [ServerController::class, "index"])->middleware(['role:root,admin']);
    # Add one server to the list : root
    Route::post('/server/add', [ServerController::class, "store"])->middleware(['role:root']);
    # Show server infos by its adress : root, admin, mailgw
    Route::get('/server/{hostname}', [ServerController::class, "show"])->middleware(['role:root,admin,mailgw']);
    # Delete a server by its adress : root
    Route::delete('/server/{hostname}', [ServerController::class, "destroy"])->middleware(['role:root']);

    # TRANSPORT
    # Add one transport : root
    Route::post('/server/{hostname}/transport/add', [TransportController::class, "store"])->middleware(['role:root']);
    # Show transport infos by its adress : root, admin, mailgw
    Route::get('/server/{hostname}/transport', [TransportController::class, "index"])->middleware(['role:root,admin,mailgw']);
    # Show all transport : root, admin, , mailgw
    Route::get('/server/all/transport', [TransportController::class, "index"])->middleware(['role:root,admin,mailgw']);
    # Delete a transport by its adress : root
    Route::delete('/server/{hostname}/transport/{id}', [TransportController::class, "destroy"])->middleware(['role:root']);

    # SPOOLER
    # Show a server's spool : root, admin, user
    Route::get('/server/{hostname}/spool', [SpoolController::class, "index"])->middleware(['role:root,admin,user']);
    # Show all spool : root, admin, user
    Route::get('/server/all/spool', [SpoolController::class, "index"])->middleware(['role:root,admin,user']);


    # CLI
    # Update cli : root, mailgw
    Route::post('/cli/{hostname}', [ServerController::class, "cli"])->middleware(['role:root,mailgw']);

});
