<?php
require_once 'vendor/autoload.php';

use GuzzleHttp\Client;

// list routes and the expected access names
$loginRoute = "/api/user/login";
$routesToTest = [
    ["/api/user/me",                    ['root','admin','user','mailgw']],
    ["/api/user/me/password",           ['root','admin','user']],
    ["/api/user/smtp-auth/list",        ['root','admin']],
    ["/api/user/smtp-auth/add",         ['root']],
    ["/api/user/smtp-auth/username",    ['root','admin']],
    ["/api/admin/user/add",             ['root']],
    ["/api/admin/user/list",            ['root']],
    ["/api/admin/user/id",              ['root']],
    ["/api/admin/user/id/log-permission",['root']],
    ["/api/log/search",                 ['root','admin','user']],
    ["/api/log/stat",                   ['root','admin','user']],
    ["/api/log",                        ['root','mailgw']],
    ["/api/log/dataTables",             ['root','admin','user']],
    ["/api/log/clean",                  ['root']],
    ["/api/user/logout",                ['root','admin','user']]
];
// list credentials and the given names
$creds = [
    ["root@retzo.com",'root','password'],
    ["admin@retzo.com",'admin','password'],
    ["user@retzo.com",'user','password'],
    ["mailgw@retzo.com",'mailgw','password']
];


//TODO tenter avec le snippet
/*
$response = Http::withBody( 
    '{
"email": "root@retzo.net",
"password": "password"
}', 'json' 
) 
->withHeaders([ 
    'User-Agent'=> 'Thunder Client (https://www.thunderclient.com)', 
    'Accept'=> 'application/json', 
    'Content-Type'=> 'application/json', 
]) 
->post('http://127.0.0.1:8000/api/user/login'); 

echo $response->body();
*/





// create a Guzzle client
$client = new Client([
    'base_uri' => "http://127.0.0.1:8000",
    'timeout' => 2.0,
    'headers' => [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json'
    ]
]);
// instantiate results table
$testResults = [];
$failTotal = 0;
// run tests for every cred : start by login
foreach ($creds as $cred) {
    $response = $client->request('POST', $loginRoute, [
        'body' => json_encode([
            'email' =>$cred[0],
            'password' =>$cred[2],
        ])
    ]);
    //run tests for every cred : use login token to try all routes
    if ($response->getStatusCode() === 200 || $response->getStatusCode() === 201) {
        $token = json_decode($response->getBody())->token;
        $testResults[$cred[0]] = [];

        foreach ($routesToTest as $route) {
            $response = $client->request('GET', $route[0], [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token
                ]
            ]);
            // analyse results
            $accessGranted = ($response->getStatusCode()===200 || $response->getStatusCode() === 201);
            $accessIsWanted = in_array($cred[1],$route[1]);
            if ($accessGranted && $accessIsWanted){
                $message = "granted, OK ";
            }
            if (!$accessGranted && !$accessIsWanted){
                $message = "blocked, OK ";
            }
            if (!$accessGranted && $accessIsWanted){
                $message = "blocked, FAIL ";
                $failTotal++;
            }
            if ($accessGranted && !$accessIsWanted){
                $message = "granted, FAIL ";
                $failTotal++;
            }
            
            $testResults[$cred[0]][$route[0]] = $message;
        }
    }
}

$totalTests = 0;
$fileName = 'test_results.txt';

foreach ($testResults as $cred => $results) {
    foreach ($results as $route => $status) {
        file_put_contents($fileName, $cred . " : " . $route . " : " . $status . "\n", FILE_APPEND);
        $totalTests++;
    }
}

file_put_contents($fileName, "Total tests : " . $totalTests . "\n", FILE_APPEND);
file_put_contents($fileName, "Total fails : " . $failTotal . "\n", FILE_APPEND);
